create DATABASE sistema;

use sistema;

create table empleados (
  id int not null AUTO_INCREMENT,
  nombre varchar(255),
  correo varchar(255),
  foto varchar(5000),
  fecha date,
  PRIMARY KEY (id)

);