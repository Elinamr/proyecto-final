from flask import Flask, render_template, request, redirect, send_from_directory, url_for, flash
from flaskext.mysql import  MySQL
from datetime import datetime
import os

app = Flask(__name__)
app.secret_key = 'Develop'

mysql = MySQL()
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'sistema'

mysql.init_app(app)


CARPETA = os.path.join('uploads/')
app.config['CARPETA'] = CARPETA

@app.route('/uploads/<nombreFoto>')
def uploads(nombreFoto):
    return send_from_directory(app.config['CARPETA'], nombreFoto)

@app.route('/')
def index():
  
  sql = "select * from empleados;"
  
  conn = mysql.connect()
  cursor = conn.cursor()
  
  cursor.execute(sql)
  empleados = cursor.fetchall()
  
  conn.commit()
  
  cursor.close()
  conn.close()
  
  return render_template('empleados/index.html', empleados=empleados)

@app.route('/destroy/<int:id>')
def destroy(id):
  
  conn = mysql.connect()
  cursor = conn.cursor()
  
  cursor.execute("select foto from empleados where id = %s", (id))
  fila = cursor.fetchall()
  
  os.remove(os.path.join(app.config['CARPETA'], fila[0][0]))
  
  cursor.execute("delete from empleados where id = %s;", (id))
  conn.commit()
  
  cursor.close()
  conn.close()
  
  return redirect('/')

@app.route('/edit/<int:id>')
def edit(id):
  
  conn = mysql.connect()
  cursor = conn.cursor()
  
  cursor.execute("select * from empleados where id=%s;", (id))
  empleados = cursor.fetchall()
  
  conn.commit()
  
  cursor.close()
  conn.close()
  
  return render_template('empleados/edit.html', empleados=empleados)

@app.route('/update', methods=['POST'])
def update():
  _nombre = request.form['txtNombre']
  _correo = request.form['txtCorreo']
  _fecha = request.form['txtFecha']
  
  _foto = request.files['txtFoto']
    
  id = request.form['txtId']
  
  sql = "update empleados set nombre=%s, correo=%s, fecha=%s where id=%s;"

  datos = (_nombre, _correo, _fecha, id)

  conn = mysql.connect()
  cursor = conn.cursor()
  
  
  now = datetime.now()
  tiempo = now.strftime('%Y%H%M%S')

  if _foto.filename != '':
    
    nuevoNombre = tiempo + _foto.filename
    _foto.save('uploads/' + nuevoNombre)
    cursor.execute("select foto from empleados where id = %s", (id))
    fila = cursor.fetchall()
    
    os.remove(os.path.join(app.config['CARPETA'], fila[0][0]))
    cursor.execute("update empleados set foto=%s where id=%s", (nuevoNombre, id))
    conn.commit()

  cursor.execute(sql, datos)
  conn.commit()

  cursor.close()
  conn.close()
    
  return redirect('/')

@app.route('/create')
def create():
    
    return render_template('empleados/create.html')


@app.route('/store', methods=['POST'])
def store():
  
  _nombre = request.form['txtNombre']
  _correo = request.form['txtCorreo']
  _fecha = request.form['txtFecha']
  
  _foto = request.files['txtFoto']
  
  if _nombre == '' or _correo == '' or _fecha == '' or _foto == '':
    flash('Recuerda llenar los datos de los campos')
    return redirect(url_for('create'))
  
  now = datetime.now()
  tiempo = now.strftime('%Y%H%M%S')

  if _foto.filename != '':
    
    nuevoNombre = tiempo + _foto.filename
    _foto.save('uploads/' + nuevoNombre)
  
  sql = "insert into empleados (nombre, correo, foto, fecha) values (%s, %s, %s, %s);"

  datos = (_nombre, _correo, nuevoNombre, _fecha)


  conn = mysql.connect()
  cursor = conn.cursor()

  cursor.execute(sql, datos)
  conn.commit()

  cursor.close()
  conn.close()

  return redirect('/')

if __name__ == '__main__':
  
  app.run(debug=True)